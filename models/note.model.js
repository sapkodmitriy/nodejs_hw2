import mongoose from 'mongoose';

const NoteShema = new mongoose.Schema(
	{
		userId: { type: String, required: true },
		completed: { type: Boolean, required: true },
		text: { type: String, required: true },
		createdDate: { type: String, required: true }
	},
	{ versionKey: false }
);

const NoteModel = mongoose.model('Note', NoteShema);

class Note {
	constructor(userId, completed, text, createdDate) {
		this.userId = userId;
		this.completed = completed;
		this.text = text;
		this.createdDate = createdDate;
	}

	static insert(note) {
		return new NoteModel(note).save();
	}

	static getById(id) {
		return NoteModel.findById({ _id: id });
	}

	static getByUserIdOffsetLimit(userId, offset, limit) {
		return NoteModel.find({ userId: userId }).skip(offset).limit(limit);
	}

	static updateNote(note) {
		return NoteModel.findByIdAndUpdate(note._id, note);
	}

	static delete(id) {
		return NoteModel.findByIdAndDelete(id);
	}

	static deleteUserNotes(userId) {
		return NoteModel.deleteMany({ userId: userId });
	}
}

export default Note;
