import User from '../models/user.model.js';
import { StatusCodes } from 'http-status-codes';

export default function checkDuplicateUsername(req, res, next) {
	const { username } = req.body;

	// Check if user with this username exist
	User.getByUsername(username)
		.then((user) => {
			if (user) {
				return res.status(StatusCodes.BAD_REQUEST).json({
					message: 'Bad request'
				});
			}

			next();
		})
		.catch(() => {
			return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
				message: 'Internal server error'
			});
		});
}
