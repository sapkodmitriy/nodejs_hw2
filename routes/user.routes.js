import express from "express";
import verifyToken from "../middleware/authJwt.js";
import {
  userProfile,
  deleteUser,
  updateUserPass,
} from "../controllers/user.controller.js";

const router = new express.Router();

router.get("/me", [verifyToken], userProfile);
router.delete("/me", [verifyToken], deleteUser);
router.patch("/me", [verifyToken], updateUserPass);

export default router;
