import express from "express";
import verifyToken from "../middleware/authJwt.js";
import {
  userNotes,
  addNote,
  userNote,
  updateNote,
  checkNote,
  deleteNote,
} from "../controllers/note.controller.js";

const router = new express.Router();

router.get("/", [verifyToken], userNotes);
router.post("/", [verifyToken], addNote);
router.get("/:id", [verifyToken], userNote);
router.put("/:id", [verifyToken], updateNote);
router.patch("/:id", [verifyToken], checkNote);
router.delete("/:id", [verifyToken], deleteNote);

export default router;
