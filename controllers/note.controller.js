import Note from '../models/note.model.js';
import { StatusCodes } from 'http-status-codes';

export async function userNotes(req, res) {
	const user = req.user;
	let { offset, limit } = req.query;
	offset = offset ? +offset : 0;
	limit = limit ? +limit : 0;

	try {
		let notes = await Note.getByUserIdOffsetLimit(user._id, offset, limit);

		res
			.status(StatusCodes.OK)
			.json({ offset, limit, count: notes.length, notes });
	} catch (err) {
		res
			.status(StatusCodes.INTERNAL_SERVER_ERROR)
			.json({ message: 'Internal server error' });
	}
}

export async function addNote(req, res) {
	const user = req.user;
	const { text } = req.body;

	try {
		await Note.insert(
			new Note(user._id, false, text, new Date().toISOString())
		);

		res.status(StatusCodes.OK).json({ message: 'Success' });
	} catch (err) {
		res
			.status(StatusCodes.INTERNAL_SERVER_ERROR)
			.json({ message: 'Internal server error' });
	}
}

export async function userNote(req, res) {
	const { id } = req.params;

	if (!id) {
		res.status(StatusCodes.OK).json({ message: 'Bad request' });
	}

	try {
		const note = await Note.getById(id);

		if (!note) {
			res.status(StatusCodes.OK).json({ message: 'Bad request' });
		}

		res.status(StatusCodes.OK).json({ note });
	} catch (err) {
		res
			.status(StatusCodes.INTERNAL_SERVER_ERROR)
			.json({ message: 'Internal server error' });
	}
}

export async function updateNote(req, res) {
	const { id } = req.params;
	const { text } = req.body;

	if (!id) {
		res.status(StatusCodes.OK).json({ message: 'Bad request' });
	}

	try {
		const note = await Note.getById(id);
		if (!note) {
			res.status(StatusCodes.OK).json({ message: 'Bad request' });
		}

		note.text = text;
		await Note.updateNote(note);

		res.status(StatusCodes.OK).json({ message: 'Success' });
	} catch (err) {
		res
			.status(StatusCodes.INTERNAL_SERVER_ERROR)
			.json({ message: 'Internal server error' });
	}
}

export async function checkNote(req, res) {
	const { id } = req.params;

	if (!id) {
		res.status(StatusCodes.OK).json({ message: 'Bad request' });
	}

	try {
		const note = await Note.getById(id);
		if (!note) {
			res.status(StatusCodes.OK).json({ message: 'Bad request' });
		}

		note.completed = !note.completed;
		await Note.updateNote(note);

		res.status(StatusCodes.OK).json({ message: 'Success' });
	} catch (err) {
		res
			.status(StatusCodes.INTERNAL_SERVER_ERROR)
			.json({ message: 'Internal server error' });
	}
}

export async function deleteNote(req, res) {
	const { id } = req.params;

	if (!id) {
		res.status(StatusCodes.OK).json({ message: 'Bad request' });
	}

	try {
		const note = await Note.getById(id);

		if (!note) {
			res.status(StatusCodes.OK).json({ message: 'Bad request' });
		}

		await Note.delete(id);

		res.status(StatusCodes.OK).json({ message: 'Success' });
	} catch (err) {
		res
			.status(StatusCodes.INTERNAL_SERVER_ERROR)
			.json({ message: 'Internal server error' });
	}
}
