import User from '../models/user.model.js';
import Note from '../models/note.model.js';
import bcrypt from 'bcryptjs';
import { StatusCodes } from 'http-status-codes';

const saltRounds = 10;

export async function userProfile(req, res) {
	const user = req.user;

	try {
		res.status(StatusCodes.OK).json({
			user: {
				_id: user._id,
				username: user.username,
				createdDate: user.createdDate
			}
		});
	} catch (err) {
		res
			.status(StatusCodes.INTERNAL_SERVER_ERROR)
			.json({ message: 'Internal server error' });
	}
}

export async function deleteUser(req, res) {
	const user = req.user;

	try {
		await User.delete(user._id);
		await Note.deleteUserNotes(user._id);

		res.status(StatusCodes.OK).json({
			message: 'Success'
		});
	} catch (err) {
		res
			.status(StatusCodes.INTERNAL_SERVER_ERROR)
			.json({ message: 'Internal server error' });
	}
}

export async function updateUserPass(req, res) {
	const user = req.user;
	const { oldPassword, newPassword } = req.body;

	try {
		const passwordIsValid = await bcrypt.compare(oldPassword, user.password);
		if (!passwordIsValid) {
			return res
				.status(StatusCodes.BAD_REQUEST)
				.json({ message: 'Bad request' });
		}

		user.password = await bcrypt.hash(newPassword, saltRounds);
		await User.updateUser(user);
		res.status(StatusCodes.OK).json({
			message: 'Success'
		});
	} catch (err) {
		res
			.status(StatusCodes.INTERNAL_SERVER_ERROR)
			.json({ message: 'Internal server error' });
	}
}
