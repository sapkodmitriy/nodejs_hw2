import User from "../models/user.model.js";
import jwt from "jsonwebtoken";
import bcrypt from "bcryptjs";
import { StatusCodes } from "http-status-codes";

const jwtSalt = process.env.JWTSECRET || "SecretJWTSalt123!@$";
const saltRounds = 10;

export async function signup(req, res) {
  const { username, password } = req.body;

  try {
    const passwordHash = await bcrypt.hash(password, saltRounds);
    await User.insert(
      new User(username, passwordHash, new Date().toISOString())
    );

    res.status(StatusCodes.OK).json({ message: "Success" });
  } catch (err) {
    res
      .status(StatusCodes.INTERNAL_SERVER_ERROR)
      .json({ message: "Internal server error" });
  }
}

export async function signin(req, res) {
  const { username, password } = req.body;

  try {
    const user = await User.getByUsername(username);
    if (!user) {
      return res
        .status(StatusCodes.BAD_REQUEST)
        .json({ message: "Bad request" });
    }

    const passwordIsValid = await bcrypt.compare(password, user.password);
    if (!passwordIsValid) {
      return res
        .status(StatusCodes.BAD_REQUEST)
        .json({ message: "Bad request" });
    }

    const token = jwt.sign({ id: user.id }, jwtSalt, {
      expiresIn: 86400, // 24 hours
    });

    res.status(StatusCodes.OK).json({
      message: "Success",
      jwt_token: token,
    });
  } catch (err) {
    res
      .status(StatusCodes.INTERNAL_SERVER_ERROR)
      .send({ message: err.message });
  }
}
